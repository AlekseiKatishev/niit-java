


public class Lab1Task1{
	class MyConst{
		static final int MAX_SIZE=100000;
	}	
	
	public static void main(String[] args){
		int result;
		int max=0;
		Recursion R=new Recursion();
		for(int i=1;i <= MyConst.MAX_SIZE;i++){
			result=R.getCnt(i);
			R.setCnt(0);
			if (result>max){
				max=result;
			}
		}
		System.out.println(max);
	}
}	


	
class Recursion{
	private int cnt=0;
	
	private void rec(int n){
		cnt++;
		if(n==1) return;
		else if (n%2==0) rec(n/2);
		else rec(3*n+1);
	}	
	
	int getCnt(int n){
		rec(n);
		return cnt;
	}
	
	void setCnt(int n){
		cnt=0;
	}
}
	
	
	
	
	
	
	
	
	
	
	