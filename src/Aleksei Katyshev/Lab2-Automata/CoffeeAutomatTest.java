package Automata;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;


public class CoffeeAutomatTest {
    CoffeeAutomat coffee=new CoffeeAutomat();

    @Test
    public void on() throws Exception {
        CoffeeAutomat obj=new CoffeeAutomat();
        obj.on();
        Assert.assertEquals(obj.getState(),"WAIT");
   }

    @Test
    public void coin() throws Exception {
    coffee.on();
    coffee.coin(10);
    Assert.assertTrue(coffee.getCash()==10);
    coffee.off();
    }

}