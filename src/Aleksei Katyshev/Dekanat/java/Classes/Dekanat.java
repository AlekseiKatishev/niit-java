package Classes;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.math.BigDecimal;
import static java.lang.System.exit;

public class Dekanat{
        private List<Group> groups=new ArrayList<Group>();
        private List<Student> students=new ArrayList<Student>();
        public Dekanat(){
            creatList("group.txt","groups");
            creatList("students.txt","students");
            fillAllGroup();
        }

        private void creatList(String fileName, String listName) {
            String[] oneStr;
            File f1 = new File(fileName);
            FileReader flrd;
            String bufStr;
            try {
                flrd = new FileReader(f1);
                BufferedReader buffered = new BufferedReader(flrd);
                while ((bufStr = buffered.readLine()) != null) {

                       if (!bufStr.isEmpty()) {
                            if (listName.equals("groups"))
                                groups.add(new Group(bufStr));
                            else if (listName.equals("students")) {
                                oneStr = bufStr.split("\t", 3);
                                Group groupStud = giveMeGroup(oneStr[2]);
                                students.add(new Student(Integer.parseInt(oneStr[0]), oneStr[1], groupStud));
                            } else {
                                throw new MyException(1);
                             }
                       }
                }
            } catch (NullPointerException ex){
                System.out.println("Null point.");
                ex.printStackTrace();
                exit(4);
            } catch (FileNotFoundException ex) {
                System.out.println("File not found.");
                ex.printStackTrace();
                exit(3);
            } catch (IOException e) {
                System.out.println("IO Exception.");
                e.printStackTrace();
                exit(2);
            } catch (MyException e) {
                e.printStackTrace();
                exit(1);
            }
        }

        /**выдаём группу по String'у*/
        private Group giveMeGroup(String name) {
            Group result=groups.get(0);
            for(Group group:groups){
              if(group.getTitle().equals(name)){
                  result=group;
              }
            }
            return result;
        }
    /**наполняем группы студентами*/
    private void fillAllGroup(){
           for(Group group:groups){
               group.fillingGroup(students);
           }
    }
    private int getNumberStud(Student student){
            int number=-1;
            for(int i=0;i<students.size();i++){
                if(students.get(i).getId()==student.getId()){
                    number=i;
                }
            }
            return number;
    }
    public List<Student> getStudents(){
        return students;
    }
    public List<Group> getGroups(){
        return groups;
    }
    public void putMarks(){
            for(Student student:students) {
                int num = (int) (Math.random() * 10) + 5;//кол-во
                for(int i=0;i<num;i++) {
                    int mark = (int) (Math.random() * 4) + 2;//оценка
                    student.setMarks(mark);
                }
            }
    }

    public void getStatistics(){
        for(Group group:groups){
            System.out.print("group: " + group.getTitle());
            System.out.println(" middle mark: " + group.calcMiddleMark());
        }
    }
    public List<Student>  transferStudent(Student student,String groupTitle){
        System.out.println(student.getFio());
        System.out.print("группа ДО перевода: "+ student.getGroupTitle());
        student.getGroup().dismissStudent(student);//берём группу в которой студент и исключаем оттуда
        student.setGroup(giveMeGroup(groupTitle));
        System.out.println(" группа ПОСЛЕ перевода: "+ student.getGroupTitle());
        giveMeGroup(groupTitle).setStudent(student);
        return students;
    }
    public void checkAcademicPerformance (){
                 Student StudBuf;

                 ListIterator<Student> iterator = students.listIterator();//удаляем из списка студентов
                 while (iterator.hasNext()) {
                      StudBuf=iterator.next();
                   if (StudBuf.calcMarks() < 3 ) {
                       System.out.println("Отчислен: " + StudBuf.getFio());
                       StudBuf.getGroup().dismissStudent(StudBuf);//берём группу в которой студент и исключаем оттуда
                       iterator.remove();
                   }
                   }
            }
    public void election(Group group){
        group.setHead();
        System.out.print("Новый староста группы "+group.getTitle());
        System.out.println(" назначен " + group.getHead().getFio());
    }

    public void writeFile(){
    File outFile=new File("outDecanat.txt");
        try {
            if(!outFile.exists()){
                outFile.createNewFile();
            }
            PrintWriter out=new PrintWriter(outFile.getAbsoluteFile());
            try{
                for(Student s:students) {
                    BigDecimal score=new BigDecimal(s.calcMarks());
                    out.print(s.getId()+" "+s.getFio()+" "+s.getGroup().getTitle()+" средний балл "+score.setScale(2,BigDecimal.ROUND_HALF_UP));
                    if(s.getGroup().getHead()==s){
                        out.println(" <<HEAD>>");
                    }
                    else{
                        out.println("");
                    }

                }
            } finally {
                out.close();
            }
        } catch (IOException e) {
            System.out.print("Can't create new file");
            e.printStackTrace();
            exit(6);
        }
    }

    class MyException extends Exception {
        private int number;
        MyException(int number){
            this.number=number;

        }
        public String toString(){
            String msg="null";
            if(number==1) {
                msg="MyException: "+number+" bad List Name";
            }
            return msg;
        }
    }
}

