package Classes;
import java.util.*;

public class Student {

    private final int id;
    private final String fio;
    private Group group;
    private List<Integer> marks=new ArrayList<Integer>();

    Student(int id, String fio,Group group){
        this.id=id;
        this.fio=fio;
        this.group=group;
    }


    void setMarks(int mark){
        marks.add(mark);
    }
    int getNum() {
        return marks.size();
    }

    int getId() {
        return id;
    }
    String getFio() {
        return fio;
    }
    String getGroupTitle(){
        return group.getTitle();
    }
    Group getGroup(){
        return group;
    }
    void setGroup(Group group) {
        this.group = group;
    }
    double calcMarks(){
        double summ=0;
        for(Integer mark:marks){
            summ+=mark;
        }
        return summ/marks.size();
    }






}
