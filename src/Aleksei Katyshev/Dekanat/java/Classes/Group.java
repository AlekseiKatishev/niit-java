package Classes;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Group {


    private String title;
    private List<Student> students=new ArrayList<Student>();
    private Student head;
    Group(String title){
        this.title=title;
    }

    private Student getStudent(double i) {//взять студента по номеру в списке(use double)
        return students.get((int)i);
    }
      public Student getStudent(String fio) {
        Student result=null;
        for(Student student:students){
            if(student.getFio().equals(fio)){
                result=student;
            }
        }
         return result;
    }
    private Student getStudent(int id) {
        Student result=null;
        for(Student student:students){
            if(student.getId()==id){
                result=student;
            }
        }
        return result;
    }
    void fillingGroup(List<Student> allStudents){
        for(Student student:allStudents){
            if(student.getGroupTitle().equals(this.title)){
                setStudent(student);
            }
        }
    }
    void setStudent(Student student) {
       students.add(student);
    }
    String getTitle() {
        return title;
    }
    private int getNumber(Student student) {
        int number=-1;
            for(int i=0;i<students.size();i++){
                if(students.get(i).getId()==student.getId()){
                    number=i;
                }
            }
            return number;
    }
    Student getHead() {
        return head;
    }
    void setHead() {
        int i= (int) (Math.random() * students.size());
        this.head = students.get(i);
    }
    double calcMiddleMark(){
        double result=0;
        for(Student student:students){
            result+=student.calcMarks();
        }
        return result/students.size();
    }
    void dismissStudent(Student student){
        ListIterator<Student> iterator = students.listIterator();
        while(iterator.hasNext()) {
            if(iterator.next()==student){
                iterator.remove();
            }
        }
    }
}
