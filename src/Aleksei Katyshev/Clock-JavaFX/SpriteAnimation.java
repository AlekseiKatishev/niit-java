package sample;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class SpriteAnimation extends Transition {
    private final  ImageView imageView;
    private final int frames;
    private final int width;
    private final int height;

    public SpriteAnimation(ImageView imageView,Duration duration, int frames,int width,int height){
        this.imageView=imageView;
        this.frames=frames;
        this.width=width;
        this.height=height;
        setCycleDuration(duration);
        setInterpolator(Interpolator.LINEAR);
        this.imageView.setViewport(new Rectangle2D(0,0,width,height));
    }

    protected void interpolate(double k){
        final int index = Math.min((int) Math.floor(k * frames), frames - 1);
        final int x = (index % frames) * width ;
        final int y = (index / frames) * height ;
        imageView.setViewport(new Rectangle2D(x, y, width, height));
    }

}


