package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.*;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.time.LocalTime;



public class Main extends Application implements Runnable{
    Thread t;
    private Pane root=new Pane();
    private Image clock= new Image(getClass().getResourceAsStream("1.png"));
    private ImageView clockView=new ImageView(clock);
    private Image hour= new Image(getClass().getResourceAsStream("Hour.png"));
    private Arrows hourArrow=new Arrows(hour,0);
    private Image sec= new Image(getClass().getResourceAsStream("Sec.png"));
    private Arrows secArrow=new Arrows(sec,0);
    private Image min=new Image(getClass().getResourceAsStream("Min.png"));
    private Arrows minArrow=new Arrows(min,0);

    public Main()  {

        (t=new Thread(this)).start();

    }

    public void run(){
        while (true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                return;
            }
            Platform.runLater(() -> {
                repaint();
                root.requestLayout();
            });

        }
    }
    @Override
    public void start(Stage primaryStage) throws Exception{
        Mechanism mechanism=new Mechanism();
        root.getChildren().addAll(mechanism,clockView);
        Scene scene =new Scene(root,540,540);
        primaryStage.setTitle("Big ben");
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                t.interrupt();
            }
        });
    }

    private void repaint(){
        LocalTime lt=LocalTime.now();

        long minutes=lt.getMinute();
        long seconds = lt.getSecond();
        long hours = lt.getHour();
        double secAngle = 6*seconds;
        double minAngle = (0.1)*(minutes*60+seconds);
        double hourAngle=(0.5)*(hours*60+minutes);
         root.getChildren().remove(secArrow);
         secArrow=new Arrows(sec,secAngle);
         root.getChildren().addAll(secArrow);
            root.getChildren().remove(minArrow);
            minArrow=new Arrows(min,minAngle);
            root.getChildren().addAll(minArrow);
                root.getChildren().remove(hourArrow);
                hourArrow=new Arrows(hour,hourAngle);
                root.getChildren().addAll(hourArrow);
    }


    public static void main(String[] args){

        launch(args);

    }


}

