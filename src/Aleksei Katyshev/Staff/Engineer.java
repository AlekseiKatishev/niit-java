
public class Engineer extends Employee implements WorkTime,Project {
    double contribution;
    Engineer(int id,String fio,String prof){
        super(id,fio,prof);
        this.project="free";
    }

    public void calcPaymentForWorkTime(){
        setPayment(PAYMENT_FOR_HOUR_PROGRAMMERS*WORK_TIME_PROGRAMMERS);
    }
    public double calcPaymentForContribution(double buget,double summ) {
        setPayment(contribution/100*buget);
        return summ-((contribution/100)*buget);
    }

    double getContribution(){
        return contribution;
    }
}

class Programmers extends Engineer {
    Programmers(int id,String fio,String prof){
        super(id,fio, prof);
        this.contribution=Math.random()*(MAX_CON_PROGRAMMER-MIN_CON_PROGRAMMER)+MIN_CON_PROGRAMMER;
    }
}

class Testers extends Engineer{
    Testers(int id,String fio,String prof){
        super(id,fio, prof);
        this.contribution=Math.random()*(MAX_CON_TESTER-MIN_CON_TESTER)+MIN_CON_TESTER;
    }
    @Override
    public void calcPaymentForWorkTime(){
        setPayment(WORK_TIME_TESTERS*PAYMENT_FOR_HOUR_TESTERS);
    }
}

class TeamLeaders extends Programmers implements Heading{
    TeamLeaders(int id,String fio,String prof){
        super(id,fio, prof);
    }

    public void calcPaymentForHeading() {
        setPayment(PAY_FOR_ONE_TL*LEADS_TL);
    }
}