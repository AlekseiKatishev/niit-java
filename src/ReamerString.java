
public class ReamerString {
   static final int MAX_SIZE = 80;
   static final int MIN_SIZE = 3;
   static char[] arrSymbol = new char[MAX_SIZE];
   static char[] arrNumber = new char[MIN_SIZE];
   static char[] arrNumberPast = new char[MIN_SIZE];
   static int arrSymbolIndex = 0;

   public static void main(String[] args) {
      ReamerString rs = new ReamerString();

      arrSymbol = args[0].toCharArray();
      for (int i = 0; arrSymbol[i] != '.'; i++) {
         rs.convertCharToInt();
      }
   }

   void convertCharToInt() {
      int counterNumber = 0;
      int nextCounterNumber = 0;
      int number = 0;
      int nextNumber = 0;
      int arrNumberIndex = 0;
      int arrNumberPastIndex = 0;
      while (arrSymbol[arrSymbolIndex] != ',' && arrSymbol[arrSymbolIndex] != '.') {
         if (arrSymbol[arrSymbolIndex] >= '0' && arrSymbol[arrSymbolIndex] <= '9') {
            arrNumber[arrNumberIndex] = arrSymbol[arrSymbolIndex];
         }
         if (arrSymbol[arrSymbolIndex] == '-') {
            arrSymbolIndex++;
            while (arrSymbol[arrSymbolIndex] != ',' && arrSymbol[arrSymbolIndex] != '.') {
               arrNumberPast[arrNumberPastIndex] = arrSymbol[arrSymbolIndex];
               arrSymbolIndex++;
               arrNumberPastIndex++;
               nextCounterNumber++;
            }
            break;
         }
         arrSymbolIndex++;
         arrNumberIndex++;
         counterNumber++;
      }
      for (int i = counterNumber - 1, j = 1; i >= 0; i--, j *= 10) {
         number += (Character.getNumericValue(arrNumber[i])) * j;
      }
      for (int i = nextCounterNumber - 1, j = 1; i >= 0; i--, j *= 10) {
         nextNumber += (Character.getNumericValue(arrNumberPast[i])) * j;
      }
      if (nextNumber > 0) {
         for (int j = number; j <= nextNumber; j++) {
            System.out.print(j);
            if (j < nextNumber)
               System.out.print(',');
         }
      } else if (number > 0) {
         System.out.print(number);
      }
      if (arrSymbol[arrSymbolIndex] == ',') {
         arrSymbolIndex++;
         System.out.print(',');
      }
   }
}
