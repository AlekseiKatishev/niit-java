class Sqrt
{
   double delta;
   double arg;

   Sqrt(double arg, double delta) {
      this.arg=arg;
	  this.delta=delta;
   }
   double average(double x,double y) {
      return (x+y)/2.0;
   }
   boolean good(double guess,double x) {
      return Math.abs(guess*guess-x)<delta;
   }
   double improve(double guess,double x) {
      return average(guess,x/guess);
   }
   double iter(double guess, double x) {
      if(good(guess,x))
         return guess;
      else
         return iter(improve(guess,x),x);
   }
   public double calc() {
      return iter(1.0,arg);
   }
}

class Program2
{
   static String Convert(double delta,double result)
   {
	   int count=0;
	   while(delta<1){
		  count++;
		  delta*=10;
	  }
	  String str_result=Double.toString(result);
	  String[] arr_str_result=str_result.split("\\.");
	  arr_str_result[1].substring(0,count);
	  str_result=arr_str_result[0].concat(".").concat(arr_str_result[1].substring(0,count));
	  return str_result;
   }
   
   public static void main(String[] args)
   {
      double val=Double.parseDouble(args[0]);
	  double delta=Double.parseDouble(args[1]);
      Sqrt sqrt=new Sqrt(val,delta);
      double result=sqrt.calc();
      String str_result=Convert(delta,result);
	  
	  System.out.println("Sqrt of "+(int)val+" = "+str_result);
   }
}