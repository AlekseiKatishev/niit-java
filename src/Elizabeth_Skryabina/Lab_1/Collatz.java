/*class MyConst {
	public static final int MAX_SIZE=100;
}*/

public class Collatz {
	
    public static long collatz(long n) {
		if (n == 1) return 1;
        else if (n % 2 == 0) return (1+collatz(n / 2));
		else return (1+collatz(3*n + 1));  
    }
	
	
    public static void main(String[] args) {
        //int N = MyConst.MAX_SIZE;
		long max_sequence=0, max_numb=0,value;
		for(int N=1;N<1000000;N=N+1)	{
			value=collatz(N);
			if (max_sequence < value){
			max_sequence = value;
			max_numb = N;
			}
		}
        System.out.println("in the range of 2-1000000 longest sequence Collatz = "+max_sequence+", with n = "+max_numb);
    }
}