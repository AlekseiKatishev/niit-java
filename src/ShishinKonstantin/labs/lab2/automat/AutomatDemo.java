package kostya;

class Automat{
    enum STATES {OFF, WAIT, ACCEPT, CHECK, COOK};	//выключено, ожидание, прием денег, проверка наличности, приготовление
    private STATES state;	//текущее состояние
    private int cash = 0;	//текущая сумма в рублях
    private String[] menu = {"Tea","Coffee","Milk","Coffee with milk","Boiling water"};		//массив названий напитков
    private int[] prices = {25,30,15,40,10};	//массив цен напитков
    private int numDrink = 0;	//номер напитка в списке

    //конструктор
    Automat() {
        state = STATES.OFF;
    }

    //методы
    public STATES on() {	//включение
        if(state==STATES.OFF) {
            state = STATES.WAIT;
            printState();
            printMenu();
        }
        return state;
    }

    public STATES off() {	//выключение
        if(state==STATES.WAIT) {
            state = STATES.OFF;
            printState();
        }
        return state;
    }

    protected String printState(){	//отображение текущего состояния для пользователя
        String strMessage = "STATE: "+state;
        System.out.println(strMessage);
        return strMessage;
    }

    protected String[] printMenu() {	//отображение меню с напитками и ценами для пользователя
        System.out.println("MENU:");
        String[] arrMenu = new String[menu.length];
        for(int i=0; i<menu.length; i++)
        {
            arrMenu[i] = (i+1)+": "+menu[i]+" - "+prices[i]+" rub.";
            System.out.println(arrMenu[i]);
        }
        return arrMenu;
    }

    public int coin(int cash) {	//занесение денег пользователем
        String strMessage = null;
        if(state==STATES.WAIT || state==STATES.ACCEPT) {
            state = STATES.ACCEPT;
            printState();
            this.cash += cash;
            strMessage = "Cash: "+this.cash+" rub.";
            System.out.println(strMessage);
        }
        return this.cash;
    }

    public String cancel() {	//отмена сеанса обслуживания пользователем
        String strMessage = null;
        if(state==STATES.ACCEPT) {
            cash = 0;
            strMessage = "Take the money!";
            System.out.println(strMessage);
            state = STATES.WAIT;
            printState();
        }
        return strMessage;
    }

    public String choice(int numDrink) {	//выбор напитка пользователем
        if(state==STATES.ACCEPT) {
            state = STATES.CHECK;
            printState();
            this.numDrink=numDrink-1;
            System.out.println("Your drink: "+menu[this.numDrink]);
            check();
        }
        return menu[this.numDrink];
    }

    protected String strReturnCheck = null;     //переменная для теста метода check()

    private String check() {	//проверка наличия необходимой суммы
        String strMessage = null;
        if(state==STATES.CHECK) {
            state = STATES.CHECK;
            printState();
            if(cash<prices[numDrink]){
                strMessage = "Need cash more!";
                strReturnCheck = strMessage;
                System.out.println(strMessage);
                state = STATES.ACCEPT;
            }
            else if(cash>prices[numDrink]) {
                strMessage = "Your change: " + (cash - prices[numDrink]) + " rub.";
                strReturnCheck = strMessage;
                System.out.println(strMessage);
                cook();
            }
            else {
                strMessage = "Money is enough.";
                strReturnCheck = strMessage;
                System.out.println(strMessage);
                cook();
            }
        }
        return strMessage;
    }

    protected STATES cook() {    //имитация процесса приготовления напитка
        if(state==STATES.CHECK) {
            state = STATES.COOK;
            printState();
            finish();
        }
        return state;
    }

    protected String strReturnFinish = null;    //переменная для теста метода finish()

    private String finish() {	//завершение обслуживания пользователя
        String strMessage = null;
        if(state==STATES.COOK) {
            strMessage = menu[this.numDrink]+" is ready!";
            strReturnFinish = strMessage;
            System.out.println(strMessage);
            state = STATES.WAIT;
            printState();
        }
        return strMessage;
    }
}

public class AutomatDemo{
    public static void main(String[] args){
        Automat automat = new Automat();
        automat.on();
        automat.coin(20);
        automat.cancel();
        automat.coin(25);
        automat.choice(2);
        automat.coin(10);
        automat.choice(2);
        automat.off();
    }
}
