public class Collatz {
    static int i=0, x=0; 
    public static void collatz(long n) {
        i++;
        if (n == 1) return;
        else if (n % 2 == 0) collatz(n / 2);
        else collatz(3*n + 1);
    }
    public static void main(String[] args) {
        for (long n=1; n<=1000000; n++)
        {
			i=0;
			collatz(n);
			if (i>x) x=i;	//находим максимальное число в последовательности Коллатца
		}
		System.out.println(x);
        
    }
}