package kostya;

import junit.framework.TestCase;

public class DekanatTest extends TestCase {

    public void testCalcAverageMarkStudent() throws Exception {
        Dekanat testDekanat = new Dekanat();
        Student[] testStudent = testDekanat.getStudents();
        testStudent[0].setMark(5);
        testStudent[0].setMark(4);
        testStudent[0].setMark(3);
        assertEquals(4.0,testStudent[0].calcAverageMarkStudent());
    }

    public void testCalcAverageMarkGroup() throws Exception {
        Dekanat testDekanat = new Dekanat();
        Student[] testStudent = testDekanat.getStudents();
        for(int i=0; i<testDekanat.getNumStudents(); i++) {
            testStudent[i].setMark(5);
            testStudent[i].setMark(4);
            testStudent[i].setMark(3);
        }
        Group[] testGroup = testDekanat.getGroups();
        assertEquals(4.0,testGroup[0].calcAverageMarkGroup());
    }
}
