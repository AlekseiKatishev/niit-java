/* Лабораторный практикум №1. Задача 3. Развертка числовых диапазонов
Написать программу, которая принимает через командную строку несколько 
числовых диапазонов и выдает на экран список чисел. 
Например: ''1,2,4-7,18-21'' -> 1,2,4,5,6,7,18,19,20,21..

 */

public class task3{
	public static void main(String[] args){
		char string[] = args[0].toCharArray();
		for(int i=0;i<string.length;i++){
			int prev=0, next=0,temp;
			while(i<string.length && string[i]!=',' && string[i]!='-'){
				temp = Character.digit(string[i],10); 
				prev = temp + prev*10;
				i++;
			}
			System.out.print(prev + ",");
			if(string[i]=='-'){
				i++;
				while(i<string.length && string[i]!=','){
					temp = Character.digit(string[i],10); 
					next = temp + next*10;
					i++;
				}
				while(prev<next)
					System.out.print(++prev + ",");
			}
		}
	}
}
