package mypackage;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by BalandinAS on 12.04.2017.
 */
public class DekanatTest
{
    Dekanat dekanat = new Dekanat("test_students.json", "test_groups.json");

    @Before
    public void before()
    {
        dekanat.fillGroups();
        dekanat.choiceHeadsInGroups();
        dekanat.randomMarks();
        dekanat.saveGroupsAndStudentsInFile("test_students_in_groups.json");
    }

    @Test
    public void choiceHeadsInGroupsTest() throws Exception
    {
        assertTrue(dekanat.searchGroup("gr-1").getHead() != null);
        assertTrue(dekanat.searchGroup("gr-2").getHead() != null);

    }

    @Test
    public void randomMarksTest() throws Exception
    {
        assertTrue(dekanat.searchGroup("gr-1").calcAverageRate() != 0);
        assertTrue(dekanat.searchGroup("gr-2").calcAverageRate() != 0);
    }

    @Test
    public void moveStudentTest() throws Exception
    {
        dekanat.moveStudent(dekanat.searchStudent(1) ,dekanat.searchGroup("gr-2"));
        assertTrue(dekanat.searchGroup("gr-1").searchStudent(1) == null);
        assertTrue(dekanat.searchGroup("gr-2").searchStudent(1) != null);
    }

    @Test
    public void fillGroupsTest() throws Exception
    {
        assertTrue(dekanat.searchGroup("gr-1").getStudents() != null);
        assertTrue(dekanat.searchGroup("gr-2").getStudents() != null);
    }

}