/**
 * Created by me on 3/24/17.
 */
public class Automata
{
    private enum STATES {OFF, WAIT, ACCEPT, CHECK, COOK};
    private int cash = 0;
    private String menu[];
    private int prices[];
    private int moneyCash[];
    private int moneyBuf[];
    //private int moneyChange[];
    private int coins[];
    private final int numberOfCoins;
    private int change = 0;
    private STATES state;

    Automata(String menu[], int prices[], int moneyCash[], int coins[])
    {
        this.numberOfCoins = coins.length;
        this.coins = coins.clone();
        sortIntArrayDesc(this.coins);
        if(!isRightMoneyCash(this.coins, moneyCash))
            throw new AutomataException("Constructor Error: Array coins or cash is wrong");
        if(menu.length != prices.length)
            throw new AutomataException("Constructor Error: Array menu or prices is wrong");
        this.menu = menu.clone();
        this.prices = prices.clone();
        this.state = STATES.OFF;
        this.moneyCash = moneyCash.clone();
        this.moneyBuf = new int[this.numberOfCoins];

    }

    private boolean isRightMoneyCash(int[] coins, int[] moneyCash)
    {
        if(moneyCash.length != coins.length)
            return false;
        for(int i = 0; i < coins.length; i++)
            if(moneyCash[i] % coins[i] != 0)
                return false;
        return true;
    }

    public String getState()
    {
        return state.toString();
    }

    public String[] getWholeMenu()
    {
        String[] printMenu = new String[this.menu.length];
        for(int i = 0; i < menu.length; i++)
        {
            printMenu[i] = i + 1 + ") " + this.menu[i] + ": " + prices[i] + "$";
        }
        return printMenu;
    }

    public String[] getMenu()
    {
        return this.menu;
    }

    public int[] getNominalsOfCoins()
    {
        return coins.clone();
    }

    private void sortIntArrayDesc(int array[])
    {
        for(int i = 0; i < array.length; i++)
            for(int j = i + 1; j < array.length; j++)
                if(array[i] < array[j])
                {
                    array[i] = array[i] + array[j];
                    array[j] = array[i] - array[j];
                    array[i] = array[i] - array[j];
                }
    }

    public void on()
    {
        if(this.state == STATES.OFF)
            this.state = STATES.WAIT;
    }

    public void off()
    {
        if(this.state == STATES.WAIT)
            this.state = STATES.OFF;
        else
            throw new AutomataException("off() State Error");
    }

    public boolean coin(int coin)
    {
        if(this.state != STATES.WAIT && this.state != STATES.ACCEPT)
            throw new AutomataException("coin() State Error");
        int i;
        if ((i = isCoin(coin)) >= 0)
        {
            this.moneyBuf[i] += coin;
            this.state = STATES.ACCEPT;
            return true;
        }
        return false;
    }

    public int choice(int numProduct)
    {
        if(this.state != STATES.ACCEPT)
            throw new AutomataException("choice() State Error");
        if(numProduct > 0 && numProduct <= menu.length)
            return numProduct - 1;
        return -1;
    }

    public boolean check(int product)
    {
        if(this.state != STATES.ACCEPT)
            throw new AutomataException("check() State Error");
        for(int c: this.moneyBuf)//calc cash
            this.cash += c;
        this.change = this.cash - this.prices[product];
        if(this.change < 0)
        {
            //this.state = STATES.WAIT;
            return false;
        }
        else
        {
            this.state = STATES.CHECK;
            return true;
        }
    }

    public void cook()
    {
        if(this.state != STATES.CHECK)
            throw new AutomataException("cook() State Error");
        this.state = STATES.COOK;
    }

    public int[] change()
    {

        if(this.state != STATES.COOK)
            throw new AutomataException("change() State Error");
        moneyBufToCash();
        int moneyChange[] = new int[this.numberOfCoins];
        int i = 0;
        while(this.change != 0)
        {
            if(this.change / this.coins[i] != 0 && this.moneyCash[i] != 0)
            {
                int sum = this.change / this.coins[i] * this.coins[i];
                this.moneyCash[i] -= sum;
                moneyChange[i] += sum;
                this.change -= sum;
            }
            i = i < this.numberOfCoins ? i + 1 : this.numberOfCoins - 1;
        }
        cash = change = 0;
        this.state = STATES.WAIT;
        return moneyChange;
    }

    public int[] cancel()
    {
        if(state != STATES.ACCEPT)
            throw new AutomataException("cancel() State Error");
        int moneyCancel[] = moneyBuf.clone();
        clearIntArr(moneyBuf);
        state = STATES.WAIT;
        return moneyCancel;

    }

    private void clearIntArr(int arr[])
    {
        for(int i = 0; i < arr.length; i++)
            arr[i] = 0;
    }

    private int isCoin(int coin)
    {
        int i = 0;
        for(int fromcoins: this.coins)
        {
            if (fromcoins == coin)
                return i;
            i++;
        }
        return -1;
    }

    private void moneyBufToCash()
    {
        int i = 0;
        for(int coins: this.moneyBuf)
        {
            this.moneyCash[i] += coins;
            this.moneyBuf[i] = 0;
            i++;
        }
    }
}

