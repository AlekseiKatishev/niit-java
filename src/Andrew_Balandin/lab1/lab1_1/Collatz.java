//Найти наибольшую последовательность Коллатца для чисел в диапазоне от 1 до 1 000 000. 

public class Collatz 
{
    public static int seq_len;
    public static int max;
    public static void collatz(long n) 
    {
        seq_len++;
        if (n == 1) return;
        else if (n % 2 == 0) collatz(n / 2);
        else collatz(3 * n + 1);
    }
    public static void main(String[] args)
    {
        max = 0;
        long max_n = 0;
        long i = 1000000;
        while(i > 1)
        {
            seq_len = 0;
            collatz(i);
            if(seq_len > max)
            {
                max = seq_len;
                max_n = i;
            }
            i--;
        }
        System.out.println("Max Collatz sequence has " + max + " members, n = " + max_n);
    }
}
