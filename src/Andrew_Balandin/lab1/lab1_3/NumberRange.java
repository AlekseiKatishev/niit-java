// Написать программу, которая принимает через командную строку несколько числовых диапазонов и выдает на экран список чисел. Например: ''1,2,4-7,18-21'' -> 1,2,4,5,6,7,18,19,20,21. 
public class NumberRange
{
    public static void main(String args[])
    {
        int i = 0;
        int prev = 0;
        int next = 0;
        char arr[] = args[0].toCharArray();
        int lenarr = arr.length;
        while(i < lenarr)
        {
            if(Character.isDigit(arr[i]))
            {
                prev = arr[i] - '0' + prev * 10;
            }
            else if(arr[i] == ',')
            {
                System.out.print(prev + ",");
                prev = 0;
            }
            else
            {
                i++;
                while(i < lenarr && Character.isDigit(arr[i]))
                    next = arr[i++] - '0' + next * 10;
                while(prev < next)
                    System.out.print(prev++ + ",");
                next = 0;
                continue;
            }
            i++;
        }
        System.out.println(prev);
    }
}
