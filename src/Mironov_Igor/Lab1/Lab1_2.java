/*
Lab1_3.
Программа вычисления квадратного корня на основе примера.
Добавлена возможность изменения точности расчетов.
*/
import java.util.Scanner;

class Sqrt{    
    double arg;
	double delta;

    Sqrt (double arg, double delta){
        this.arg=arg;
		this.delta=delta;
    }
    double average(double x, double y){
        return (x+y)/2.0;
    }
    boolean good (double guess, double x){
        return Math.abs(guess*guess-x)<delta;
    }
    double improve (double guess, double x){
        return average(guess,x/guess);
    }
    double iter(double guess, double x){
        if(good(guess, x))
            return guess;
        else
            return iter(improve(guess,x),x);
    }
    public double calc() {
        return iter(1.0, arg);
    }
}

public class Lab1_2{

    public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		
        System.out.println("Enter the number: ");
		double val = in.nextDouble();
		
		System.out.println("Enter the precision: ");
		double delta = in.nextDouble();
		
        Sqrt sqrt = new Sqrt(val, delta);
		
        double result = sqrt.calc();
        System.out.println("Sqrt of "+ val +"=" + result);
    }
}