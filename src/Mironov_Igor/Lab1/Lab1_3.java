/*
Lab1_3. 
Программа, которая принимает через командную строку
несколько числовых диапазонов и выдает на экран список чисел.
*/

import java.util.Scanner;

public class Lab1_3{
	
	public static int getNumber (char[] arr, int index){		
		StringBuilder result = new StringBuilder();				
		if( (index-1 >= 0) && Character.isDigit(arr[index-1])){
			while((index >= 0) && (arr[index] != ',')){	
			    result.append(arr[index]);
				index--;
			}	
            result.reverse();			
			return Integer.parseInt(result.toString());
		}			
		else if ((index+1 < arr.length) && Character.isDigit(arr[index+1])){
			while((index < arr.length) && (arr[index] != ',')){
			    result.append(arr[index]);			
				index++;                			
			}			
			return Integer.parseInt(result.toString());
		}		
		else return Character.getNumericValue(arr[index]);	
	}
	
	public static String envolvent (String str){
		StringBuilder completedList = new StringBuilder();		
		char[] arr_str = str.toCharArray();
		for(int i = 0; i < arr_str.length; i++){		
			if(arr_str[i]=='-'){				
				completedList.append(',');
				int min = getNumber(arr_str,i-1) + 1;
				int max = getNumber(arr_str,i+1);
			    for(int j = min; j < max; j++){				
				    completedList.append(Integer.toString(j));
				    completedList.append(',');
			    }
			}
			else completedList.append(arr_str[i]);				
		}		
		return completedList.toString();
	}
	
	public static void main (String[] args){		
		Scanner in = new Scanner(System.in);		
		System.out.print("Enter the list: \t");
		String line = envolvent(in.nextLine());
		System.out.println("Envolvent:\t\t" + line);		
	}
}