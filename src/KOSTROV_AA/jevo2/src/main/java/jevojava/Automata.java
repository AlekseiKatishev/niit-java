package jevojava;

/**
 * Created by Alexander on 17.03.2017.
 */
public class Automata {

    private int cash;
    public int index;
    private String[] menu = {"Чай" , "Кофе" , "Горячий шоколад", "Глинтвейн"};
    private int[] prices = {50, 80, 100, 250};
    enum STATIC  {OFF, WAIT, ACCEPT, CHECK, COOK}
    private STATIC state = STATIC.OFF;
    private String statusS="";  // Вывод статуса автомата для пользователя в удобном виде
    private String menuS=""; // Меню автомата для пользователя в удобном виде

    public Automata(){

    }


    public int getCash(){
        return cash;
    }
    public String getUserChoice(){ return menu[index]; }
    public int getPrice(){ return prices[index]; }
    String getStatusS() { return statusS; }


    public void on(){
      this.state = STATIC.WAIT;
      }

    public void off(){
        this.state = STATIC.OFF;
       }

    public void coin(int cash){
            this.state = STATIC.ACCEPT;
            this.printState();
            this.cash = cash;
    }

    public String printMenu(){
        menuS += "Напитки: \n";
        for (int i = 0; i < menu.length; i++){
           menuS += i+1 + "." + menu[i] +  " : " + prices[i] + "\n";
        }
        return menuS;
    }

    public STATIC getState(){
        return state;
    }

    public String printState(){

        switch (state) {
            case OFF:
                statusS = "Автомат временно не работает:)";
                break;

            case WAIT:
                statusS = "Автомат включён, выбирайте напиток";
            //    this.printMenu();
                break;

            case ACCEPT:
                statusS = "Приём денег...";
                break;

            case CHECK:
                statusS = "Обработка данных... Подождите...";
                break;

            case COOK:
                statusS = "Приготовление..." + menu[index] + "...";
                break;

            default:
                statusS = "Автомат включён";
                break;
        }
        return statusS;
    }

    public void choice(int index ){
        this.state = STATIC.CHECK;
        this.printState();
        this.index = index;
       }

    public void check(){
        // если денег меньше стоимости заказанного напитка, cancel
        if (cash < prices[index])
            this.cancel();
        else
            this.cook();
       }

    public void cancel(){
        this.state = STATIC.WAIT;
        this.printState();

    }

    public void cook(){
        this.state = STATIC.COOK;
        this.printState();
        }

    public void finish(){
       this.state = STATIC.WAIT;
       this.printState();
    }

}
