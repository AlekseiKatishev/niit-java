/**
 * Created by Alexander on 12.03.2017.
 */
public class Program {
    public static void main(String[] args)
    {
        double val=Double.parseDouble(args[0]);
        double val2 = Double.parseDouble(args[1]);
        Sqrt sqrt=new Sqrt(val, val2);
        double result=sqrt.calc();
        System.out.println("Sqrt of "+val+"="+result);
    }
}
