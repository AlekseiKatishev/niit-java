/**
 * Created by Alexander on 15.03.2017.
 */
public class Circle {
    private double Radius;
    private double Ference;
    private double Area;
    private static final double pi = 3.141592653589793238462643;

    public void setRadius(double Radius){
      this.Radius = Radius;
      this.Area = pi * Math.pow(Radius, 2);
      this.Ference = 2 * pi * Radius;
    }

    public void setFerence(double Ference){
      this.Ference = Ference;
      this.Radius = Ference/(2 * pi);
      this.Area = pi * Math.pow(Radius, 2);
    }

    public void setArea(double Area){
      this.Area = Area;
      this.Radius = Math.sqrt(Area/pi);
      this.Ference = 2 * pi * Radius;
    }

    public double getRadius(){
        return this.Radius;
    }

    public double getFerence(){
        return this.Ference;
    }

    public double getArea(){
        return this.Area;
    }

}
