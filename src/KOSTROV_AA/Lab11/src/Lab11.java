/**
 * Created by HP Envy 13-d106ur on 12.03.2017.
 */
public class Lab11 {
    static int kolvo=0;
    public static int kollatc(long x){
        if (x==1) {
            return kolvo;
        }
        else if(x % 2==0){
            kolvo++;
            kollatc(x/2);
        }
        else{
            kolvo++;
            kollatc(3*x + 1);
        };
        return kolvo;
    };
    public static void main(String[] args){
        int maxi=0;
        long max_value=0;
        int current=0;
        for (long i=1;i<1000000;i++){
            kolvo = 0;
            current = kollatc(i);
            if (current>maxi) {
                maxi = current;
                max_value = i;
            }
        };
        System.out.println("наибольшая последовательность: " + maxi + " число: " + max_value);

    };
};
