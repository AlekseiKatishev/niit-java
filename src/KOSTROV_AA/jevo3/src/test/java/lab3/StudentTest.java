package lab3;

import junit.framework.TestCase;

import java.util.Arrays;

/**
 * Created by Alexander on 09.04.2017.
 */
public class StudentTest extends TestCase {
    Student student = new Student(1,"Костров.А.А");
    Group group = new Group("Археологи",4);

    public void testGetGroup() throws Exception {
        student.setGroup(group);
        String actual = "Археологи";
        String result = student.getGroup().getTitle();
        assertEquals("Тест на группу провален", actual, result);
    }

    public void testGetFio() throws Exception {
        String actual = "Костров.А.А";
        String result = student.getFio();
        assertEquals("Тест на ФИО провален", actual, result);

    }

    public void testGetNum() throws Exception {
        int actual = 2;
        student.addMark(0,5);
        student.addMark(1,5);
        int result = student.getNum();
        assertEquals("Тест на количество оценок провален", actual, result);

    }

    public void testGetId() throws Exception {
        int actual = 1;
        int result = student.getId();
        assertEquals("Тест на айди студента", actual, result);

    }

    public void testGetMarks() throws Exception {
        int[] actual = {5,5,0,0,0};
        student.addMark(0,5);
        student.addMark(1,5);
        int[] result = student.getMarks();
        assertTrue("Тест на оценки провален", Arrays.equals(actual,result));

    }


    public void testAddMark() throws Exception {
        int[] actual = {5,5,0,0,0};
        student.addMark(0,5);
        student.addMark(1,5);
        int[] result = student.getMarks();
        assertTrue("Тест на добавление оценки", Arrays.equals(actual,result));


    }

    public void testAverageMark() throws Exception {
        float actual = 4;
        student.addMark(0,3);
        student.addMark(1,5);
        student.addMark(2,3);
        student.addMark(3,5);

        float  result = student.averageMark();
        assertEquals("Тест на среднюю оценку", actual, result);

    }

}