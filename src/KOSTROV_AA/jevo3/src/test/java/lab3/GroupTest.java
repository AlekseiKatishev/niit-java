package lab3;

import junit.framework.TestCase;

/**
 * Created by Alexander on 11.04.2017.
 */
public class GroupTest extends TestCase {
    Group group = new Group("Археологи",4);
    Student student = new Student(1,"Костров.А.А");


    public void testGetId() throws Exception {
        int actual = 4;
        int result = group.getId();
        assertEquals("Тест на айди  группы провален", actual, result);

    }

    public void testGetHead() throws Exception {

        group.setHead(student);
        Student result = group.getHead();
        assertTrue("Тест на главу провален", result.equals(student));

    }

    public void testGetTitle() throws Exception {
        String actual = "Археологи";
        String result = group.getTitle();
        assertEquals("Тест на название группы провален", actual, result);
    }

    public void testAddStudent() throws Exception {

    }

    public void testSearch() throws Exception {

    }

    public void testGetAverageMarks() throws Exception {

    }

}